import { BrowserRouter, Route, Switch } from "react-router-dom";
import Footer from "./components/global/Footer";
import Navbar from "./components/global/Navbar";
import NotFound from "./components/global/NotFound";
import Profile from "./components/global/Profile";
import Home from "./components/pages/Home";
import Contact from "./components/pages/Contact";
import Resources from "./components/pages/Resources";
import Projects from "./components/pages/Projects/Projects";

function App() {
    return (
        <BrowserRouter>
            <div className="app">
                <Navbar />
                <div className="app_container">
                    <Profile />
                    <Switch>
                        <Route path="/" exact component={Home} />
                        <Route path="/projects" exact component={Projects} />
                        <Route path="/resources" exact component={Resources} />
                        <Route path="/contact" exact component={Contact} />
                        <Route component={NotFound} />
                    </Switch>
                </div>
            </div>
            <Footer />
        </BrowserRouter>
    );
}

export default App;
