import React, {useState} from 'react';
import { resources } from "../../data/resources";
import ItemModal from '../../global/Modal';
import ResourcesTable from '../../cards/Resources';

const Resources = () => {
    const [toggle, setToggle] = useState(false);
    const clickModal = () => {
        setToggle(!toggle);
    }

    return (
        <section className="resources">
            <div className="resources_text">
                <h1>Mes ressources</h1>
                <p>
                    Voici les principales ressources que j'utilisent lorsque je suis sur un projet, autre que les plus connus
                    tels que W3school, Mdn, Youtube etc. Ces ressources sont plutôt sur du design, des inspirations de site web,
                    mais aussi d'aide en Javascript.
                </p>
                <div className="resources_table">
                    <ResourcesTable data={resources}/>
                </div>
            </div>
            <div className="roadmap">
                <h1>Ma roadmap 2020/2021</h1>
                <p>
                    J'ai commencé à apprendre React, mais en continuant je me suis aperçue que je rencontrais
                    des difficultés et qu'il fallait avoir un minimum une base solide en Javascript.
                    C'est ainsi que j'ai décidé de suivre cette roadmap et de recommencer depuis le début,
                    en partant de la base HTML/CSS/JS pour ensuiter monter en compétence et en complexité.
                </p>
                <div className="roadmap_img">
                    <small>(Cliquer sur l'image pour mieux voir)</small>
                    <div className="image">
                        <img src="/assets/projects/roadmap.png" alt="roadmap" onClick={() => clickModal()}/>
                    </div>
                </div>
                {toggle && <ItemModal closeModal={{toggle, setToggle}} className="none"/>}
            </div>
        </section>
    );
};

export default Resources;