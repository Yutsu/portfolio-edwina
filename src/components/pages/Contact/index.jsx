import React, { useState } from 'react';
import "aos/dist/aos.css";

const Contact = () => {
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [message, setMessage] = useState("");

    const errorMsg = (value, formType, msgError) => {
        if(!value){
            formType.style.display = "block";
            formType.innerHTML = "Champ requis *";
            msgError.innerHTML = "Merci de remplir les champs nécessaires";
        } else {
            formType.style.display = "none";
        }
    }

    const validateMail = (name, email, message) => {
        let mail = document.querySelector('.contact_not_mail');
        const regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

        const msgError = document.querySelector('.form_message');
        const formName = document.querySelector('.contact_not_name');
        const formMsg = document.querySelector('.contact_not_msg');

        errorMsg(name, formName, msgError);
        errorMsg(message, formMsg, msgError);

        if(email.match(regex)){
            mail.style.display = "none";
            msgError.innerHTML = "";
            return true;
        } else if(!email) {
            mail.style.display = "block";
            mail.innerHTML = "Email vide *";
            msgError.innerHTML="Merci de remplir les champs nécessaires.";
            return false;
        } else {
            mail.style.display = "block";
            mail.innerHTML = "Email erreur *";
            msgError.innerHTML="Merci de remplir les champs nécessaires.";
            return false;
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        if(validateMail(name, email, message)){
            sendFeedback("template_dxu6qsn", {
                name, email, message
            });
        }
    };

    const sendFeedback = (templateId, variables) => {
        window.emailjs
            .send("service_1wj4xbi", templateId, variables)
            .then((res) => {
                console.log("success" + res);
                setName("");
                setEmail("");
                setMessage("");
            })
            .catch((err) =>
                document.querySelector('.form_message').innerHTML =
                "Une erreur s'est produite, veuillez réessayer." + err
            )
    };

    return (
        <section className="contact">
            <div className="contact_img" data-aos="fade-left">
                <img src="/assets/general/contact.svg" alt="contact me with outlook"/>
            </div>
            <form className="contact_form" onSubmit={handleSubmit} data-aos="fade-right">
                <div className="input_fullname">
                    <input
                        type="text"
                        id="fullname"
                        name="fullname"
                        onChange={(e) => setName(e.target.value)}
                        placeholder="Nom *"
                        value={name}
                        autoComplete="off"
                    />
                    <small className="contact_not_name"></small>
                </div>
                <div className="input_email">
                    <input
                        type="email"
                        id="email"
                        name="email"
                        onChange={(e) => setEmail(e.target.value)}
                        placeholder="Adresse mail *"
                        value={email}
                        autoComplete="off"
                    />
                    <small className="contact_not_mail"></small>
                </div>
                <div className="textarea_msg">
                    <textarea
                        id="message"
                        name="message"
                        onChange={(e) => setMessage(e.target.value)}
                        placeholder="Message *"
                        value={message}
                    />
                    <small className="contact_not_msg"></small>
                </div>
                <div className="contact_btn">
                    <input
                        className="button"
                        type="submit"
                        value="Envoyer"
                    />
                </div>
                <div className="form_message"></div>
            </form>
        </section>
    );
};

export default Contact;