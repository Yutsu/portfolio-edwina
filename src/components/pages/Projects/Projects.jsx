import React from 'react';
import ProjectCard from '../../cards/Projects';
import { landingPage, javascript, react } from "../../data/projects";

const mapProject = (type) => {
    return <div className="project_grid">
        {type.map(data => (
            <ProjectCard data={data} key={data.id} />
        ))}
    </div>
}

const Projets = () => {
    window.scrollTo({
        top: 0,
        left: 0,
    });

    return (
        <section className="projects">
            <h1>Mes projets</h1>
            <p>
                Voici tous les projets que j'ai réalisé de Novembre jusqu'en Juin 2021.
                J'ai tout d'abord commencé par m'emtraîner sur le binome HTML/CSS pour avoir une bonne base solide
                en faisant une multitude de landing page afin de m'améliorer dessus. Après cela,
                j'ai continué avec Javascript en manipulant le Dom et en utilisant des Api gratuites.
                Avec une bonne base en HTML/CSS/JS, cela m'a permis de commencer le framework React.
                J'ai suivi une Roadmap que je montre dans l'onglet Ressources.
                Après React je souhaiterai apprendre React native, VueJS et également Python.
            </p>
            <div className="projects_all">
                <h2 className="line">Landing page</h2>
                {mapProject(landingPage)}

                <h2 className="line">Javascript</h2>
                {mapProject(javascript)}

                <h2 className="line">React</h2>
                {mapProject(react)}
            </div>
        </section>
    );
};

export default Projets;