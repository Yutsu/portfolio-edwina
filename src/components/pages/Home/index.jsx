import React, { useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import exportCard from '../../cards/Home';
import { cardInfo, imgSkill, projectHome } from '../../data/home';
import Aos from 'aos';
import "aos/dist/aos.css";

const {InfoCard, ProjectCard} = exportCard;

const Home = () => {
    useEffect(() => {
        Aos.init({duration: 800});
    }, []);

    return (
        <section className="home">
            <div className="home_about">
                <h1>Profil</h1>
                <p>
                    Passionnée par l'informatique depuis le lycée, je souhaiterai me spécialiser dans le web (de préférence en Front-End).
                    En effet, lors de mes deux premiers stages je m'occupais principalement de l'interface graphique.
                    Cela m'a permis de découvrir l'univers du design, des animations et j'ai appréciée coder ces tâches-là.
                </p><br/>
                <p>
                    Néanmoins je n'ai pas pu trouver d'alternance en Front-end en 2020/2021 car j'avais assez peu d'expériences et surtout aucun projet à montrer.
                    J'ai donc décidé de prendre une année sabbatique en pour effectuer des projets et améliorer / acquérir de nouvelles compétence telles Javascript et React.
                    Ce portfolio a donc pour but de vous présenter tous les projets personnels que j'ai réalisé ainsi que les ressources que j'utilisent et qui m'aident pour mes projets.
                    Si vous avez des questions à me poser, n'hésitez pas à me contacter sur l'onglet Contact ou par mail.
                </p>
            </div>
            <div className="home_cards">
                {cardInfo.map(info => (
                    <InfoCard data={info} key={info.id} />
                ))}
            </div>
           <div className="home_techno">
                <h1>Compétences</h1>
                <p>
                    Voici les compétences que j'ai acquis lors de mes expériences professionnelles, personnelles et aussi en école.
                    Je ne pense pas maitrîser à la perfection toutes ses technologies mais je pense être assez à l'aise dessus.
                </p>
                <div className="home_techno_img">
                    {imgSkill.map((skill, key) => (
                        <img src={skill.img} alt="img skill" key={key}/>
                    ))}
                </div>
            </div>
            <div className="home_best-project">
                <h1>Mes projets préférés</h1>
                {projectHome.map(project => (
                    <ProjectCard data={project} key={project.id} />
                ))}
            </div>
            <div className="home_btn_project">
                <NavLink to="/projects"> Voir plus</NavLink>
            </div>
        </section>
    );
};

export default Home;