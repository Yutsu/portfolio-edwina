import React from 'react';
import { AiFillGitlab } from 'react-icons/ai';
import PropTypes from 'prop-types';

const InfoCard = ({data : {info, title, img}}) => {
    return (
        <div className="homecard">
            <img src={img} alt={info}/>
            <span>{info}</span>
            <h3>{title}</h3>
        </div>
    );
};

const ProjectCard = ({data}) => {
    const { id, name, description, img, skill, codeDemo } = data;

    return (
        <div className="projectcard">
            <div className="left" data-aos="fade-right">
                <img src={img} alt={name}/>
            </div>
            <div className="right" data-aos="fade-left">
                <h2>0{id}. {name}</h2>
                <div className="description"><p>{description}</p></div>
                <div className="skill">
                    <p>{skill.join(", ")}</p>
                </div>
                {codeDemo.map((one, key) => (
                    <div className="demoCode" key={key}>
                        <a href={one.demo} target="_blank" rel="noreferrer noopener">
                            <img src="/assets/projects/pc.png" alt="click to go to the website"/>
                        </a>
                        <a href={one.code} target="_blank" rel="noreferrer noopener">
                            <AiFillGitlab />
                        </a>
                    </div>
                )) }
            </div>
        </div>
    )
}

InfoCard.propTypes = {
    data: PropTypes.shape({
        info: PropTypes.string,
        title: PropTypes.string,
        img: PropTypes.string
    })
}

ProjectCard.propTypes = {
    data: PropTypes.shape({
        id: PropTypes.number,
        name: PropTypes.string,
        description: PropTypes.string,
        img: PropTypes.string,
        skill: PropTypes.arrayOf(PropTypes.string),
        codeDemo: PropTypes.array
    })
}

const exportCard = {
    InfoCard,
    ProjectCard
}

export default exportCard;