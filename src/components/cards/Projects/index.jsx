import React from 'react';
import PropTypes from 'prop-types';
import { AiFillGitlab } from 'react-icons/ai';

const ProjectCard = ({data}) => {
    const { id, name, skill, img, codeDemo } = data;

    return (
        <div className="project" data-aos="fade-down">
            <div className="img_website">
                <img src={img} alt={name}/>
                {codeDemo.map((one, key) => (
                    <div className="demoCode" key={key}>
                        <div className="demo">
                            <a href={one.demo}  target="_blank" label="button" rel="noreferrer noopener">
                                <img src="/assets/projects/pc.png" alt="click to go to the website"/>
                            </a>
                        </div>
                        <div className="code">
                            <a href={one.code} target="_blank" label="button" rel="noreferrer noopener">
                                <AiFillGitlab />
                            </a>
                        </div>
                    </div>
                )) }
            </div>
            <div className="info_website">
                <h3>{id}. {name}</h3>
                <div className="skill">
                    <span>{skill.join(", ")}</span>
                </div>
            </div>
        </div>
    );
};

ProjectCard.propTypes = {
    data: PropTypes.shape({
        id: PropTypes.number,
        name: PropTypes.string,
        skill: PropTypes.arrayOf(PropTypes.string),
        img: PropTypes.string,
        codeDemo: PropTypes.array
    })
}

export default ProjectCard;