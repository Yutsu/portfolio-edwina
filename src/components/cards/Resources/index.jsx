import React from 'react';
import PropTypes from 'prop-types';

const Resources = ({data}) => {
    return (
        <div className="resources">
            <table>
                <thead>
                    <tr>
                        <th className="mobile"><h3>Year</h3></th>
                        <th><h3>Name</h3></th>
                        <th className="max"><h3>Description</h3></th>
                        <th className="mobile"><h3>Founder</h3></th>
                        <th><h3>Link</h3></th>
                    </tr>
                </thead>
                {data.map((resource, key )=> (
                    <tbody key={key}>
                        <tr>
                            <>
                            <td className="mobile">{resource.year}</td>
                            <td>{resource.name}</td>
                            <td className="max">{resource.description}</td>
                            <td className="mobile">{resource.founder}</td>
                            <td>
                                <a href={resource.link} target="_blank" rel="noreferrer noopener">
                                    <img className="img_link" src="/assets/general/link.png" alt="link and go to the website"/>
                                </a>
                            </td>
                            </>
                        </tr>
                    </tbody>
                    ))}
            </table>
        </div>
    );
};

Resources.propTypes = {
    data : PropTypes.array
}

export default Resources;