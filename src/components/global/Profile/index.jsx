import React from 'react';
import { FaRegCalendarAlt, FaLinkedin } from 'react-icons/fa';
import { RiTimeLine } from 'react-icons/ri';
import { AiFillGitlab } from 'react-icons/ai';
import { GrLocation } from 'react-icons/gr';
import { BiMailSend } from 'react-icons/bi';
import  pdf  from './CValternance-EdwinaTan.pdf';

const Profile = () => {
    return (
        <div className="profile" data-aos="fade-up">
            <img src="assets/general/photo.jpg" alt="my profile identity"/>
            <h2>Edwina Tan</h2>
            <div className="btn_container">
                <div className="btn_research">
                    <p>Recherche une alternance en Front-End</p>
                </div>
            </div>
            <div className="profile_social-network">
                <a href="https://www.linkedin.com/in/edwina-tan-977bb5159"
                    target="_blank" label="button" rel="noreferrer noopener"><FaLinkedin /></a>
                <a href="https://gitlab.com/Yutsu"
                    target="_blank" label="button" rel="noreferrer noopener"><AiFillGitlab /></a>
            </div>
            <div className="profile_information">
                <p><FaRegCalendarAlt /> Disponibilité début juillet 2022 </p>
                <p><RiTimeLine /> 4 jours entreprise / 1 jour école </p>
                <p><GrLocation /> Paris, France</p>
                <p><BiMailSend /> edwina.tan@hotmail.fr </p>
            </div>
            <div className="profile_pdf">
                <a href={pdf} target="_blank" rel="noreferrer noopener">
                    <img src="/assets/general/pdf.png" alt=""/>
                </a>
            </div>
        </div>
    );
};

export default Profile;