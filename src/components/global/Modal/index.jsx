import React from 'react';
import { FaGithub } from 'react-icons/fa';
import { AiOutlineCloseSquare } from 'react-icons/ai';

const ItemModal = ({closeModal: { toggle, setToggle }}) => {

    const modalClose = () => {
        setToggle(!toggle);
    }
    return (
        <div className="modal">
            <div className="modal_container">
                <div className="modal_text">
                    <h3>Adam Golab</h3>
                    <a href="https://github.com/adam-golab/react-developer-roadmap" target="_blank" rel="noreferrer noopener">
                        <FaGithub />
                    </a>
                    <div className="close" onClick={modalClose}>
                        <AiOutlineCloseSquare />
                    </div>
                </div>
                <div className="modal_img">
                    <img src="/assets/projects/roadmap.png" alt="my roadmap that I follow since november 2020"/>
                </div>
            </div>
        </div>
    );
};

export default ItemModal;