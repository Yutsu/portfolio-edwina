import React from 'react';

const NotFound = () => {
    return (
        <section className="notFound">
            <h1>Not found !</h1>
            <img src="/assets/general/error.svg" alt="error page link"/>
        </section>
    );
};

export default NotFound;