import React from 'react';

const Footer = () => {
    return (
        <footer>
            <h4>
                <a className="hover" href="https://gitlab.com/Yutsu/portfolio-edwina" rel="noreferrer noopener"target="_blank">
                    Conçu et réalisé par Edwina Tan - 2022
                </a>
            </h4>
        </footer>
    );
};

export default Footer;