import React from 'react';
import { NavLink } from 'react-router-dom';

const Navbar = () => {
    return (
        <nav>
            <div className="nav-container">
                <ul>
                    <NavLink
                        exact={true} to="/"
                        activeClassName='active'
                        className="hover"
                    >
                        Accueil
                    </NavLink>

                    <NavLink
                        to="/projects"
                        activeClassName='active'
                        className="hover"
                    >
                        Projets
                    </NavLink>

                    <NavLink
                        to="/resources"
                        activeClassName='active'
                        className="hover"
                    >
                        Ressources
                    </NavLink>

                    <NavLink to="/contact"
                        activeClassName='active'
                        className="hover"
                    >
                        Contact
                    </NavLink>
                </ul>
            </div>
        </nav>
    );
};

export default Navbar;