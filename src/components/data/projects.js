export const landingPage = [
  {
    id: 1,
    name: "FyloDark",
    img: "assets/projects/fylodark.png",
    skill: [
      "Html",
      "Scss",
      "Javascrip",
      "Responsive",
      "UI/UX",
      "Gsap",
      "ScrollTrigger",
    ],
    codeDemo: [
      {
        demo: "https://fylodark-edwina.netlify.app/",
        code: "https://gitlab.com/Yutsu/fylodark",
      },
    ],
  },
  {
    id: 2,
    name: "GridOne",
    img: "assets/projects/gridOne.png",
    skill: ["Html", "Css", "Responsive", "UI/UX", "Grid"],
    codeDemo: [
      {
        demo: "https://grid-one-edwina.netlify.app/",
        code: "https://gitlab.com/Yutsu/gridone",
      },
    ],
  },
  {
    id: 3,
    name: "GridTwo",
    img: "assets/projects/gridTwo.png",
    skill: ["Html", "Css", "Responsive", "UI/UX", "Grid"],
    codeDemo: [
      {
        demo: "https://grid-two-edwina.netlify.app/",
        code: "https://gitlab.com/Yutsu/gridtwo",
      },
    ],
  },
  {
    id: 4,
    name: "Clipboard",
    img: "assets/projects/clipboard.png",
    skill: ["Html", "Css", "Responsive", "UI/UX"],
    codeDemo: [
      {
        demo: "https://clipboard-edwina.netlify.app/",
        code: "https://gitlab.com/Yutsu/clipboard",
      },
    ],
  },
  {
    id: 5,
    name: "PingComing",
    img: "assets/projects/ping.png",
    skill: ["Html", "Css", "Javascript", "Responsive", "UI/UX"],
    codeDemo: [
      {
        demo: "https://ping-edwina.netlify.app/",
        code: "https://gitlab.com/Yutsu/ping",
      },
    ],
  },
  {
    id: 6,
    name: "BaseGirl",
    img: "assets/projects/baseGirl.jpg",
    skill: ["Html", "Css", "Javascript", "Responsive", "UI/UX"],
    codeDemo: [
      {
        demo: "https://basegirl-edwina.netlify.app/",
        code: "https://gitlab.com/Yutsu/basegirl",
      },
    ],
  },
  {
    id: 7,
    name: "Huddle",
    img: "assets/projects/huddle.png",
    skill: [
      "Html",
      "Css",
      "Javascript",
      "Responsive",
      "UI/UX",
      "IntersectionObserver  ",
    ],
    codeDemo: [
      {
        demo: "https://huddle-edwina.netlify.app/",
        code: "https://gitlab.com/Yutsu/huddle",
      },
    ],
  },
  {
    id: 8,
    name: "FyloWhite",
    img: "assets/projects/fylowhite.png",
    skill: ["Html", "Css", "Responsive", "UI/UX"],
    codeDemo: [
      {
        demo: "https://fylowhite-edwina.netlify.app/",
        code: "https://gitlab.com/Yutsu/fylowhite",
      },
    ],
  },
  {
    id: 9,
    name: "Bookmark",
    skill: ["Html", "Css", "Javascript", "UI/UX", "Responsive"],
    img: "/assets/projects/homeBookmark.png",
    codeDemo: [
      {
        demo: "https://bookmark-edwina.netlify.app/",
        code: "https://gitlab.com/Yutsu/bookmark",
      },
    ],
  },
];

export const javascript = [
  {
    id: 10,
    name: "Calculator",
    img: "assets/projects/calculator.png",
    skill: ["Html", "Css", "Javascript", "Responsive", "Class"],
    codeDemo: [
      {
        demo: "https://calculator-edwina.netlify.app/",
        code: "https://gitlab.com/Yutsu/calculatorapp",
      },
    ],
  },
  {
    id: 11,
    name: "Countdown",
    img: "assets/projects/countdown.png",
    skill: ["Html", "Css", "Javascript", "Responsive", "Grid", "Date"],
    codeDemo: [
      {
        demo: "https://countdown-edwina.netlify.app/",
        code: "https://gitlab.com/Yutsu/countdownapp",
      },
    ],
  },
  {
    id: 12,
    name: "EssentialMobileApp",
    img: "assets/projects/essentialMobile.png",
    skill: ["Html", "Css", "Javascript", "Responsive"],
    codeDemo: [
      {
        demo: "https://essential-app-edwina.netlify.app/",
        code: "https://gitlab.com/Yutsu/essentialmobileapp",
      },
    ],
  },
  {
    id: 13,
    name: "Password generator",
    img: "assets/projects/password.png",
    skill: ["Html", "Css", "Javascript", "Responsive"],
    codeDemo: [
      {
        demo: "https://password-edwina.netlify.app/",
        code: "https://gitlab.com/Yutsu/passwordgeneratorapp",
      },
    ],
  },
  {
    id: 14,
    name: "Post it",
    img: "assets/projects/postit.png",
    skill: ["Html", "Css", "Javascript", "Local storage", "Responsive"],
    codeDemo: [
      {
        demo: "https://postit-edwina.netlify.app/",
        code: "https://gitlab.com/Yutsu/postitapp",
      },
    ],
  },
  {
    id: 15,
    name: "Switch Games",
    img: "assets/projects/switchGame.png",
    skill: ["Html", "Css", "Javascript", "Responsive"],
    codeDemo: [
      {
        demo: "https://switch-games-list.netlify.app",
        code: "https://gitlab.com/Yutsu/switchgameapp",
      },
    ],
  },
  {
    id: 16,
    name: "Country flag",
    img: "assets/projects/countryFlag.png",
    skill: [
      "Html",
      "Scss",
      "Javascript",
      "Responsive",
      "UI/UX",
      "restcountriesAPI",
    ],
    codeDemo: [
      {
        demo: "https://countriesjsapi-edwina.netlify.app/",
        code: "https://gitlab.com/Yutsu/countriesapi",
      },
    ],
  },
  {
    id: 17,
    name: "Weather city",
    img: "assets/projects/weather.png",
    skill: ["Html", "Scss", "Javascript", "Responsive", "openweathermapAPI"],
    codeDemo: [
      {
        demo: "https://weatherjsapi-edwina.netlify.app/",
        code: "https://gitlab.com/Yutsu/weatherjsapi",
      },
    ],
  },
  {
    id: 18,
    name: "Mooflix",
    skill: [
      "Html",
      "Scss",
      "Javascript",
      "Gsap",
      "Local storage",
      "themoviedbAPI",
    ],
    img: "/assets/projects/homeMooflix.png",
    codeDemo: [
      {
        demo: "https://moviejsapi-edwina.netlify.app/",
        code: "https://gitlab.com/Yutsu/movieapi",
      },
    ],
  },
];

export const react = [
  {
    id: 19,
    name: "Country flag react",
    skill: ["React", "Scss", "Axios", "Hooks", "restcountriesAPI"],
    img: "/assets/projects/countryReact.png",
    codeDemo: [
      {
        demo: "https://country-edwina.netlify.app/",
        code: "https://gitlab.com/Yutsu/reactcountries",
      },
    ],
  },
  {
    id: 20,
    name: "Gamecoin website",
    skill: ["React", "Styled-components", "Axios", "Hooks", "restcountriesAPI"],
    img: "/assets/projects/gamecoin.png",
    codeDemo: [
      {
        demo: "https://gamecoin-edwina.netlify.app/",
        code: "https://gitlab.com/Yutsu/reactgamecoinwebsite",
      },
    ],
  },
  {
    id: 21,
    name: "TastyWorld",
    skill: ["React", "Scss", "Axios", "Hooks", "Swiper", "theMealdbAPI"],
    img: "/assets/projects/homeTastyworld.png",
    codeDemo: [
      {
        demo: "https://tastyworld-edwina.netlify.app/",
        code: "https://gitlab.com/Yutsu/reacttastyworld",
      },
    ],
  },
  {
    id: 22,
    name: "Portfolio !",
    skill: ["React", "Scss", "Hooks", "Responsive", "EmailJS"],
    img: "/assets/projects/portfolio.png",
    codeDemo: [
      {
        demo: "https://portfolio-edwina.netlify.app/",
        code: "https://gitlab.com/Yutsu/portfolio-edwina",
      },
    ],
  },
  {
    id: 23,
    name: "Todolist redux",
    skill: ["React", "Redux", "Hooks", "Responsive", "UI/UX"],
    img: "/assets/projects/todoRedux.png",
    codeDemo: [
      {
        demo: "https://todoreduxhooks-edwina.netlify.app/",
        code: "https://gitlab.com/Yutsu/reactreduxtodolist",
      },
    ],
  },
];
