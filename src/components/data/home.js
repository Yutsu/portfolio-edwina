export const cardInfo = [
  {
    id: 1,
    info: "J'étudie à",
    title: "ECV Digital",
    img: "assets/info/education.png",
  },
  {
    id: 2,
    info: "Je suis sous",
    title: "Mac",
    img: "assets/info/appleBlack.png",
  },
  {
    id: 3,
    info: "Je joue très souvent à la",
    title: "Switch",
    img: "assets/info/nintendo-switch.png",
  },
  {
    id: 4,
    info: "J'entretiens un",
    title: "Bullet journal",
    img: "assets/info/diary.png",
  },
  {
    id: 5,
    info: "J'aime beacoup",
    title: "Lire des romans",
    img: "assets/info/student.png",
  },
  {
    id: 6,
    info: "Je possède un",
    title: "Iphone 8",
    img: "assets/info/apple.png",
  },
];

export const imgSkill = [
  { img: "/assets/skill/html.png" },
  { img: "/assets/skill/css.png" },
  { img: "/assets/skill/sass.png" },
  { img: "/assets/skill/javascript.png" },
  { img: "/assets/skill/react.png" },
  { img: "/assets/skill/nodejs.png" },
  { img: "/assets/skill/php.png" },
  { img: "/assets/skill/laravel.png" },
  { img: "/assets/skill/mysql.png" },
  { img: "/assets/skill/git.png" },
];

export const projectHome = [
  {
    id: 1,
    name: "SwitchGames",
    description:
      "Ce projet est un de mes projets préférés car j'aime beaucoup la Switch. Plusieurs de mes amis me demandaient les jeux que j'avais car j'achète " +
      "pas mal de jeux, dont certains qui ne sont pas très connus. J'ai alors décidé de faire une liste de tous les jeux Switch que je possède, que je mettrai à jour, " +
      "pour que mes amis puissent voir les jeux que j'ai pris.",
    skill: ["Html", "Css", "Javascript"],
    img: "/assets/projects/switchGame.png",
    codeDemo: [
      {
        demo: "https://switch-games-list.netlify.app/",
        code: "https://gitlab.com/Yutsu/switchgameapp",
      },
    ],
  },
  {
    id: 2,
    name: "Mooflix",
    description:
      "Ce projet m'est venue à l'esprit car j'aime regarder des films grâce aux " +
      "nombreux services à disposition tels que Netflix, Prime vidéo ou encore Disney+. J'ai donc voulu avoir " +
      "mon propre 'Allocine' et qui est plus à mon goût au niveau esthétique. De plus cela m'a permis d'apprendre " +
      "le Javascript tout en travaillant avec une Api.",
    skill: ["Html", "Css", "Javascript", "TheMovieDbApi", "Gsap"],
    img: "/assets/projects/homeMooflix.png",
    codeDemo: [
      {
        demo: "https://moviejsapi-edwina.netlify.app/",
        code: "https://gitlab.com/Yutsu/movieapi",
      },
    ],
  },
  {
    id: 3,
    name: "TastyWorld",
    description:
      "C'est le premier projet que j'ai réalisé en React et avec une Api. " +
      "C'est toujours intéressant de voir des plats du monde et pourquoi pas les préparer. " +
      "Petit disclaimer, le projet a quelque soucis lorsqu'on rajoute un plat en favoris, c'est en cours de débogage.",
    skill: ["React", "Scss", "Axios", "TheMealDbAPI", "Swiper"],
    img: "/assets/projects/homeTastyworld.png",
    codeDemo: [
      {
        demo: "https://tastyworld-edwina.netlify.app/",
        code: "https://gitlab.com/Yutsu/reacttastyworld",
      },
    ],
  },
];
